package playercommands;

import com.aionemu.gameserver.configs.administration.CommandsConfig;
import com.aionemu.gameserver.configs.main.CustomConfig;
import com.aionemu.gameserver.model.Race;
import com.aionemu.gameserver.model.gameobjects.player.Player;
import com.aionemu.gameserver.model.ingameshop.InGameShopEn;
import com.aionemu.gameserver.network.aion.serverpackets.SM_SYSTEM_MESSAGE;
import com.aionemu.gameserver.network.loginserver.LoginServer;
import com.aionemu.gameserver.network.loginserver.serverpackets.SM_ACCOUNT_TOLL_INFO;
import com.aionemu.gameserver.services.events.*;
import com.aionemu.gameserver.utils.*;
import com.aionemu.gameserver.utils.chathandlers.PlayerCommand;
import com.aionemu.gameserver.world.World;

import java.util.Iterator;

/**
 * Created by wanke on 13/02/2017.
 */

public class cmd_pk extends PlayerCommand
{
    public cmd_pk() {
        super("pk");
    }
	
    @Override
    public void execute(Player player, String... params) {
        if (!player.isBandit()) {
            long toll = player.getClientConnection().getAccount().getToll();
                if (toll > CustomConfig.TOLL_PK_COST) {
                    toll -= CustomConfig.TOLL_PK_COST;
                    if(toll < 0)
                        toll = 0;
                    if (LoginServer.getInstance().sendPacket(new SM_ACCOUNT_TOLL_INFO(toll, player.getClientConnection().getAccount().getLuna(), player.getAcountName()))) {
                        player.getClientConnection().getAccount().setToll(toll);
                    }
                }else {
						PacketSendUtility.sendMessage(player, "You dont have anough toll");
                        return;
                    }
            BanditService.getInstance().startBandit(player);
            PacketSendUtility.sendSys3Message(player, "\uE005", "<[PK] Bandit> started !!!");

            Iterator<Player> iter = World.getInstance().getPlayersIterator();
            String message = "Player "+ player.getName() +" became a PK monster in "+ player.getWorldType().name() +", end his insanity to win "+ CustomConfig.TOLL_PK_COST+ " toll bounty;";
            Player target;
            while (iter.hasNext()) {
                target = iter.next();

                PacketSendUtility.sendBrightYellowMessageOnCenter(target, message);
            }
        } else {
            BanditService.getInstance().stopBandit(player);
            PacketSendUtility.sendSys3Message(player, "\uE005", "<[PK] Bandit> stop !!!");
        }
    }
}