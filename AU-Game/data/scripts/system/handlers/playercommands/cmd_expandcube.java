package playercommands;

import com.aionemu.gameserver.model.gameobjects.player.Player;
import com.aionemu.gameserver.services.CubeExpandService;
import com.aionemu.gameserver.utils.PacketSendUtility;
import com.aionemu.gameserver.utils.chathandlers.PlayerCommand;

/**
 * @author Ghostfur
 */

public class cmd_expandcube extends PlayerCommand {

    public cmd_expandcube() {
        super("expandcube");
    }

	@Override
	public void execute(Player player, String... params) {
		if (player.getNpcExpands() >= 15) {
			PacketSendUtility.sendMessage(player, "You have fully unlocked the cube!\nThere is no more cubes to unlock!");
			return;
		}
        while (player.getNpcExpands() < 15) {
            CubeExpandService.expand(player, true);
        }
		PacketSendUtility.sendMessage(player, "You have fully unlocked the cube!\nThere is no more cubes to unlock!");
	}
	
	@Override
	public void onFail(Player admin, String message) {
		PacketSendUtility.sendMessage(admin, "Syntaxe : .expandcube");
	}
}
