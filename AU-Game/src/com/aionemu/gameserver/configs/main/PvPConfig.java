package com.aionemu.gameserver.configs.main;

import com.aionemu.commons.configuration.Property;

public class PvPConfig
{

  @Property(key="gameserver.pvp.chainkill.time.restriction", defaultValue="0")
  public static int CHAIN_KILL_TIME_RESTRICTION;

  @Property(key="gameserver.pvp.chainkill.number.restriction", defaultValue="30")
  public static int CHAIN_KILL_NUMBER_RESTRICTION;

  @Property(key="gameserver.pvp.max.leveldiff.restriction", defaultValue="9")
  public static int MAX_AUTHORIZED_LEVEL_DIFF;

  @Property(key="gameserver.pvp.medal.rewarding.enable", defaultValue="false")
  public static boolean ENABLE_MEDAL_REWARDING;

  @Property(key="gameserver.pvp.medal.reward.chance", defaultValue="10")
  public static float MEDAL_REWARD_CHANCE;

  @Property(key="gameserver.pvp.medal.reward.quantity", defaultValue="1")
  public static int MEDAL_REWARD_QUANTITY;

  @Property(key="gameserver.pvp.killingspree.enable", defaultValue="false")
  public static boolean ENABLE_KILLING_SPREE_SYSTEM;

  @Property(key="gameserver.pvp.raw.killcount.spree", defaultValue="20")
  public static int SPREE_KILL_COUNT;

  @Property(key="gameserver.pvp.special_reward.type", defaultValue="0")
  public static int GENOCIDE_SPECIAL_REWARDING;

  @Property(key="gameserver.pvp.special_reward.chance", defaultValue="2")
  public static float SPECIAL_REWARD_CHANCE;


  /**
   * Killing Spree System
   */

  @Property(key = "gameserver.pvp.raw.killcount.carnage", defaultValue = "10")
  public static int CARNAGE_KILL_COUNT;

  @Property(key = "gameserver.pvp.raw.killcount.genocide", defaultValue = "15")
  public static int GENOCIDE_KILL_COUNT;

  @Property(key = "gameserver.pvp.raw.killcount.rampage", defaultValue = "20")
  public static int RAMPAGE_KILL_COUNT;

  @Property(key = "gameserver.pvp.raw.killcount.dominating", defaultValue = "25")
  public static int DOMINATING_KILL_COUNT;

  @Property(key = "gameserver.pvp.raw.killcount.unstoppable", defaultValue = "30")
  public static int UNSTOPPABLE_KILL_COUNT;

  @Property(key = "gameserver.pvp.raw.killcount.insanemonster", defaultValue = "35")
  public static int INSANEMONSTER_KILL_COUNT;

  @Property(key = "gameserver.pvp.raw.killcount.godlike", defaultValue = "40")
  public static int GODLIKE_KILL_COUNT;

  @Property(key = "gameserver.pvp.raw.killcount.wickedsick", defaultValue = "45")
  public static int WICKEDSICK_KILL_COUNT;

  @Property(key = "gameserver.pvp.raw.killcount.muthafakaaas", defaultValue = "50")
  public static int MUTHAFAKAAAS_KILL_COUNT;

  /**
   * SPREE REWARDING
   */

  @Property(key = "gameserver.pvp.spree_reward1.count", defaultValue = "10")
  public static int SPREE_REWARD_COUNT1;

  @Property(key = "gameserver.pvp.spree_reward2.count", defaultValue = "10")
  public static int SPREE_REWARD_COUNT2;

  @Property(key = "gameserver.pvp.spree_reward1.item", defaultValue = "166020000")
  public static int SPREE_REWARD_ITEM1;

  @Property(key = "gameserver.pvp.spree_reward2.item", defaultValue = "166030005")
  public static int SPREE_REWARD_ITEM2;

  /*
    PVP GP Reward Per Rank
     */

  @Property(key = "gameserver.gp.reward.rank9", defaultValue = "100")
  public static int GP_REWARD_RANK9;

  @Property(key = "gameserver.gp.reward.rank8", defaultValue = "200")
  public static int GP_REWARD_RANK8;

  @Property(key = "gameserver.gp.reward.rank7", defaultValue = "250")
  public static int GP_REWARD_RANK7;

  @Property(key = "gameserver.gp.reward.rank6", defaultValue = "275")
  public static int GP_REWARD_RANK6;

  @Property(key = "gameserver.gp.reward.rank5", defaultValue = "300")
  public static int GP_REWARD_RANK5;

  @Property(key = "gameserver.gp.reward.rank4", defaultValue = "325")
  public static int GP_REWARD_RANK4;

  @Property(key = "gameserver.gp.reward.rank3", defaultValue = "350")
  public static int GP_REWARD_RANK3;

  @Property(key = "gameserver.gp.reward.rank2", defaultValue = "375")
  public static int GP_REWARD_RANK2;

  @Property(key = "gameserver.gp.reward.rank1", defaultValue = "400")
  public static int GP_REWARD_RANK1;

  @Property(key = "gameserver.gp.reward.off1", defaultValue = "450")
  public static int GP_REWARD_OFF1;

  @Property(key = "gameserver.gp.reward.off2", defaultValue = "475")
  public static int GP_REWARD_OFF2;

  @Property(key = "gameserver.gp.reward.off3", defaultValue = "500")
  public static int GP_REWARD_OFF3;

  @Property(key = "gameserver.gp.reward.off4", defaultValue = "525")
  public static int GP_REWARD_OFF4;

  @Property(key = "gameserver.gp.reward.off5", defaultValue = "550")
  public static int GP_REWARD_OFF5;

  @Property(key = "gameserver.gp.reward.gen", defaultValue = "575")
  public static int GP_REWARD_GEN;

  @Property(key = "gameserver.gp.reward.ggen", defaultValue = "600")
  public static int GP_REWARD_G_GEN;

  @Property(key = "gameserver.gp.reward.comm", defaultValue = "675")
  public static int GP_REWARD_COMM;

  @Property(key = "gameserver.gp.reward.gov", defaultValue = "900")
  public static int GP_REWARD_GOV;

  /**
   * PvP GP Reward for PvP
   */
  @Property(key = "gameserver.enable.gp.reward", defaultValue = "false")
  public static boolean ENABLE_GP_REWARD;

  /**
   * Enable Toll Reward for PvP
   */
  @Property(key = "gameserver.pvp.toll.rewarding.enable", defaultValue = "false")
  public static boolean ENABLE_TOLL_REWARD;

  @Property(key = "gameserver.pvp.toll.reward.chance", defaultValue = "50")
  public static int TOLL_CHANCE;

  @Property(key = "gameserver.pvp.toll.reward.quantity", defaultValue = "5")
  public static int TOLL_QUANTITY;

  /**
   * CUSTOM TAGS
   */

  //PK Mode
  @Property(key = "gameserver.pk.tag", defaultValue = "\u2620 %s")
  public static String TAG_PK;
  //PvE Mode
  @Property(key = "gameserver.pve.tag", defaultValue = "\u26E8 %s")
  public static String TAG_PVE;

}