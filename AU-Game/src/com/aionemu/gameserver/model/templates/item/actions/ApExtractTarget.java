package com.aionemu.gameserver.model.templates.item.actions;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;

@XmlType(name="ApExtractTarget")
@XmlEnum
public enum ApExtractTarget
{
  ACCESSORY,  ARMOR,  EQUIPMENT,  WEAPON,  WING,  OTHER,  ALL;
  
  private ApExtractTarget() {}
  
  public String value()
  {
    return name();
  }
  
  public static ApExtractTarget fromValue(String paramString)
  {
    return valueOf(paramString);
  }
}
