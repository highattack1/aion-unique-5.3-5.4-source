package com.aionemu.gameserver.model.templates.item.actions;

import com.aionemu.gameserver.controllers.ObserveController;
import com.aionemu.gameserver.controllers.PlayerController;
import com.aionemu.gameserver.controllers.observer.ItemUseObserver;
import com.aionemu.gameserver.model.DescriptionId;
import com.aionemu.gameserver.model.TaskId;
import com.aionemu.gameserver.model.gameobjects.Item;
import com.aionemu.gameserver.model.gameobjects.player.Player;
import com.aionemu.gameserver.model.items.storage.Storage;
import com.aionemu.gameserver.model.templates.item.ItemTemplate;
import com.aionemu.gameserver.model.templates.item.ItemUseLimits;
import com.aionemu.gameserver.network.aion.serverpackets.SM_INVENTORY_UPDATE_ITEM;
import com.aionemu.gameserver.network.aion.serverpackets.SM_ITEM_USAGE_ANIMATION;
import com.aionemu.gameserver.network.aion.serverpackets.SM_SYSTEM_MESSAGE;
import com.aionemu.gameserver.utils.PacketSendUtility;
import com.aionemu.gameserver.utils.ThreadPoolManager;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name="RemoveSoulBindAction")
public class RemoveSoulBindAction
  extends AbstractItemAction
{
  public RemoveSoulBindAction() {}
  
  public boolean canAct(Player paramPlayer, Item paramItem1, Item paramItem2)
  {
    return true;
  }
  
  public void act(final Player paramPlayer, final Item paramItem1, final Item paramItem2)
  {
    final int i = paramItem1.getItemId();
    final int j = paramItem1.getObjectId();
    PacketSendUtility.broadcastPacket(paramPlayer, new SM_ITEM_USAGE_ANIMATION(paramPlayer.getObjectId(), paramItem1.getObjectId(), i, 5000, 0, 0), true);
    final ItemUseObserver local1 = new ItemUseObserver()
    {
      public void abort()
      {
        paramPlayer.getController().cancelTask(TaskId.ITEM_USE);
        paramPlayer.removeItemCoolDown(paramItem1.getItemTemplate().getUseLimits().getDelayId());
        PacketSendUtility.sendPacket(paramPlayer, new SM_SYSTEM_MESSAGE(1300427));
        PacketSendUtility.broadcastPacket(paramPlayer, new SM_ITEM_USAGE_ANIMATION(paramPlayer.getObjectId(), j, i, 0, 2, 0), true);
        paramPlayer.getObserveController().removeObserver(this);
      }
    };
    paramPlayer.getObserveController().attach(local1);
    paramPlayer.getController().addTask(TaskId.ITEM_USE, ThreadPoolManager.getInstance().schedule(new Runnable()
    {
      public void run()
      {
        paramPlayer.getObserveController().removeObserver(local1);
        PacketSendUtility.broadcastPacket(paramPlayer, new SM_ITEM_USAGE_ANIMATION(paramPlayer.getObjectId(), j, i, 0, 1, 1), true);
        if (!paramPlayer.getInventory().decreaseByObjectId(j, 1L)) {
          return;
        }
        paramItem2.setSoulBound(false);
        PacketSendUtility.sendPacket(paramPlayer, new SM_SYSTEM_MESSAGE(1403332, new DescriptionId(paramItem2.getNameId())));
        PacketSendUtility.sendPacket(paramPlayer, new SM_INVENTORY_UPDATE_ITEM(paramPlayer, paramItem2));
      }
    }, 100L));
  }
}
