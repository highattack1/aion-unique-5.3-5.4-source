package com.aionemu.gameserver.model.templates.item.actions;

import com.aionemu.commons.utils.Rnd;
import com.aionemu.gameserver.controllers.ObserveController;
import com.aionemu.gameserver.controllers.PlayerController;
import com.aionemu.gameserver.controllers.observer.ItemUseObserver;
import com.aionemu.gameserver.model.TaskId;
import com.aionemu.gameserver.model.gameobjects.Item;
import com.aionemu.gameserver.model.gameobjects.PersistentState;
import com.aionemu.gameserver.model.gameobjects.player.Equipment;
import com.aionemu.gameserver.model.gameobjects.player.Player;
import com.aionemu.gameserver.model.items.storage.Storage;
import com.aionemu.gameserver.model.stats.container.PlayerGameStats;
import com.aionemu.gameserver.model.templates.item.ItemTemplate;
import com.aionemu.gameserver.model.templates.item.ItemUseLimits;
import com.aionemu.gameserver.network.aion.serverpackets.SM_ITEM_USAGE_ANIMATION;
import com.aionemu.gameserver.network.aion.serverpackets.SM_SYSTEM_MESSAGE;
import com.aionemu.gameserver.services.item.ItemPacketService;
import com.aionemu.gameserver.utils.PacketSendUtility;
import com.aionemu.gameserver.utils.ThreadPoolManager;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name="AuthorizeAction")
public class AuthorizeAction
  extends AbstractItemAction
{
  @XmlAttribute(name="count")
  private int count;
  private boolean success;
  
  public AuthorizeAction() {}
  
  public boolean canAct(Player paramPlayer, Item paramItem1, Item paramItem2)
  {
    if (paramItem2.getItemTemplate().getAuthorize() == 0) {
      return false;
    }
    if (paramItem2.getAuthorize() >= paramItem2.getItemTemplate().getAuthorize()) {
      return false;
    }
    return (!paramItem2.getItemTemplate().isBracelet()) || (paramItem2.getAuthorize() < 10);
  }
  
  public void act(final Player paramPlayer, final Item paramItem1, final Item paramItem2)
  {
    PacketSendUtility.broadcastPacketAndReceive(paramPlayer, new SM_ITEM_USAGE_ANIMATION(paramPlayer.getObjectId(), paramItem2.getObjectId(), paramItem1.getObjectId(), paramItem1.getItemId(), 5000, 0, 0));
    final ItemUseObserver local1 = new ItemUseObserver()
    {
      public void abort()
      {
        paramPlayer.getController().cancelTask(TaskId.ITEM_USE);
        paramPlayer.removeItemCoolDown(paramItem1.getItemTemplate().getUseLimits().getDelayId());
        PacketSendUtility.sendPacket(paramPlayer, SM_SYSTEM_MESSAGE.STR_MSG_ITEM_AUTHORIZE_CANCEL(paramItem2.getNameId()));
        PacketSendUtility.broadcastPacket(paramPlayer, new SM_ITEM_USAGE_ANIMATION(paramPlayer.getObjectId(), paramItem1.getObjectId(), paramItem1.getItemId(), 0, 3, 0));
        paramPlayer.getObserveController().removeObserver(this);
      }
    };
    paramPlayer.getObserveController().attach(local1);
    final boolean bool = isSuccess(paramPlayer);
    paramPlayer.getController().addTask(TaskId.ITEM_USE, ThreadPoolManager.getInstance().schedule(new Runnable()
    {
      public void run()
      {
        paramPlayer.getObserveController().removeObserver(local1);
        if (paramPlayer.getInventory().decreaseByItemId(paramItem1.getItemId(), 1L)) {
          if (!bool)
          {
            success = false;
            paramItem2.setAuthorize(0);
            if (paramItem2.getItemTemplate().isBracelet())
            {
              paramItem2.setOptionalSocket(0);
              PacketSendUtility.sendPacket(paramPlayer, SM_SYSTEM_MESSAGE.STR_MSG_ITEM_AUTHORIZE_FAILED(paramItem2.getNameId()));
            }
            if (paramItem2.getItemTemplate().isPlume()) {
              PacketSendUtility.sendPacket(paramPlayer, SM_SYSTEM_MESSAGE.STR_STR_MSG_ITEM_AUTHORIZE_FAILED_TSHIRT(paramItem2.getNameId()));
            } else {
              PacketSendUtility.sendPacket(paramPlayer, SM_SYSTEM_MESSAGE.STR_MSG_ITEM_AUTHORIZE_FAILED(paramItem2.getNameId()));
            }
          }
          else
          {
            success = true;
            paramItem2.setAuthorize(paramItem2.getAuthorize() + 1);
            PacketSendUtility.sendPacket(paramPlayer, SM_SYSTEM_MESSAGE.STR_MSG_ITEM_AUTHORIZE_SUCCEEDED(paramItem2.getNameId(), paramItem2.getAuthorize()));
          }
        }
        if (paramItem2.getItemTemplate().isBracelet()) {
          switch (paramItem2.getAuthorize())
          {
          case 5: 
            paramItem2.setOptionalSocket(1);
            break;
          case 7: 
            paramItem2.setOptionalSocket(2);
            break;
          case 10: 
            paramItem2.setOptionalSocket(3);
            break;
          }
        }
        paramPlayer.getController().cancelTask(TaskId.ITEM_USE);
        paramPlayer.getGameStats().updateStatsVisually();
        if (paramItem2.isEquipped()) {
          paramPlayer.getEquipment().setPersistentState(PersistentState.UPDATE_REQUIRED);
        } else {
          paramPlayer.getInventory().setPersistentState(PersistentState.UPDATE_REQUIRED);
        }
        ItemPacketService.updateItemAfterInfoChange(paramPlayer, paramItem2);
        if (!success) {
          PacketSendUtility.broadcastPacketAndReceive(paramPlayer, new SM_ITEM_USAGE_ANIMATION(paramPlayer.getObjectId(), paramItem1.getObjectId(), paramItem1.getItemId(), 0, 2, 0));
        } else {
          PacketSendUtility.broadcastPacketAndReceive(paramPlayer, new SM_ITEM_USAGE_ANIMATION(paramPlayer.getObjectId(), paramItem1.getObjectId(), paramItem1.getItemId(), 0, 1, 0));
        }
      }
    }, 5000L));
  }
  
  public boolean isSuccess(Player paramPlayer)
  {
    int i = Rnd.get(0, 1000);
    if ((i < 700) && (paramPlayer.getAccessLevel() > 2))
    {
      PacketSendUtility.sendMessage(paramPlayer, "Success! Rnd: " + i);
      return true;
    }
    PacketSendUtility.sendMessage(paramPlayer, "Fail! Rnd: " + i);
    return false;
  }
}
