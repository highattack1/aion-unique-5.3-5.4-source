package com.aionemu.gameserver.model.templates.item.actions;

import com.aionemu.gameserver.controllers.ObserveController;
import com.aionemu.gameserver.model.gameobjects.Item;
import com.aionemu.gameserver.model.gameobjects.player.Player;
import com.aionemu.gameserver.model.gameobjects.player.PlayerCommonData;
import com.aionemu.gameserver.model.items.storage.Storage;
import com.aionemu.gameserver.model.templates.item.ItemTemplate;
import com.aionemu.gameserver.network.aion.serverpackets.SM_ITEM_USAGE_ANIMATION;
import com.aionemu.gameserver.network.aion.serverpackets.SM_SYSTEM_MESSAGE;
import com.aionemu.gameserver.utils.PacketSendUtility;
import javax.xml.bind.annotation.XmlAttribute;

public class ExpAction
  extends AbstractItemAction
{
  @XmlAttribute
  protected int cost;
  @XmlAttribute(name="percent")
  protected boolean isPercent;
  
  public ExpAction() {}
  
  public boolean canAct(Player paramPlayer, Item paramItem1, Item paramItem2)
  {
    return cost > 0;
  }
  
  public void act(Player paramPlayer, Item paramItem1, Item paramItem2)
  {
    long l1 = paramPlayer.getCommonData().getExpNeed();
    long l2 = isPercent ? l1 * cost / 100L : cost;
    if (paramPlayer.getInventory().decreaseByObjectId(paramItem1.getObjectId(), 1L))
    {
     // paramPlayer.getCommonData().setExp(paramPlayer.getCommonData().getExp() + l2); // Todo
      paramPlayer.getObserveController().notifyItemuseObservers(paramItem1);
      PacketSendUtility.sendPacket(paramPlayer, SM_SYSTEM_MESSAGE.STR_GET_EXP2(l2));
      ItemTemplate localItemTemplate = paramItem1.getItemTemplate();
      PacketSendUtility.broadcastPacket(paramPlayer, new SM_ITEM_USAGE_ANIMATION(paramPlayer.getObjectId(), paramItem1.getObjectId(), localItemTemplate.getTemplateId()), true);
    }
  }
}
