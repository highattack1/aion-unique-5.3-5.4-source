package com.aionemu.gameserver.model.templates.item.actions;

import com.aionemu.gameserver.model.gameobjects.Item;
import com.aionemu.gameserver.model.gameobjects.player.Player;
import javax.xml.bind.annotation.XmlAttribute;

public class RemodelAction
  extends AbstractItemAction
{
  @XmlAttribute(name="type")
  private int extractType;
  @XmlAttribute(name="minutes")
  private int expireMinutes;
  
  public RemodelAction() {}
  
  public boolean canAct(Player paramPlayer, Item paramItem1, Item paramItem2)
  {
    return false;
  }
  
  public void act(Player paramPlayer, Item paramItem1, Item paramItem2) {}
  
  public int getExpireMinutes()
  {
    return expireMinutes;
  }
  
  public int getExtractType()
  {
    return extractType;
  }
}
