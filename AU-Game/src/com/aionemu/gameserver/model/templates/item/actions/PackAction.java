package com.aionemu.gameserver.model.templates.item.actions;

import com.aionemu.commons.network.util.ThreadPoolManager;
import com.aionemu.gameserver.controllers.ObserveController;
import com.aionemu.gameserver.controllers.PlayerController;
import com.aionemu.gameserver.controllers.observer.ItemUseObserver;
import com.aionemu.gameserver.model.DescriptionId;
import com.aionemu.gameserver.model.TaskId;
import com.aionemu.gameserver.model.gameobjects.Item;
import com.aionemu.gameserver.model.gameobjects.PersistentState;
import com.aionemu.gameserver.model.gameobjects.player.Player;
import com.aionemu.gameserver.model.items.storage.Storage;
import com.aionemu.gameserver.model.templates.item.ItemQuality;
import com.aionemu.gameserver.model.templates.item.ItemTemplate;
import com.aionemu.gameserver.model.templates.item.ItemUseLimits;
import com.aionemu.gameserver.network.aion.serverpackets.SM_INVENTORY_UPDATE_ITEM;
import com.aionemu.gameserver.network.aion.serverpackets.SM_ITEM_USAGE_ANIMATION;
import com.aionemu.gameserver.network.aion.serverpackets.SM_SYSTEM_MESSAGE;
import com.aionemu.gameserver.utils.PacketSendUtility;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name="PackAction")
public class PackAction
  extends AbstractItemAction
{
  @XmlAttribute
  protected UseTarget target;
  
  public PackAction() {}
  
  public boolean canAct(Player paramPlayer, Item paramItem1, Item paramItem2)
  {
    if ((target.equals(UseTarget.WEAPON)) && (!paramItem2.getItemTemplate().isWeapon())) {
      return false;
    }
    if ((target.equals(UseTarget.ARMOR)) && (!paramItem2.getItemTemplate().isArmor())) {
      return false;
    }
    if (paramItem2.getOptionalSocket() == -1)
    {
      PacketSendUtility.sendPacket(paramPlayer, new SM_SYSTEM_MESSAGE(1402030));
      return false;
    }
    if (paramItem2.isEquipped())
    {
      PacketSendUtility.sendPacket(paramPlayer, new SM_SYSTEM_MESSAGE(1402020));
      return false;
    }
    if (paramItem2.isTradeable(paramPlayer))
    {
      PacketSendUtility.sendPacket(paramPlayer, new SM_SYSTEM_MESSAGE(1402022));
      return false;
    }
    if ((paramItem1.getItemTemplate().getItemQuality() != paramItem2.getItemTemplate().getItemQuality()) && (paramItem1.getItemTemplate().getItemQuality().getQualityId() < paramItem2.getItemTemplate().getItemQuality().getQualityId()))
    {
      PacketSendUtility.sendPacket(paramPlayer, new SM_SYSTEM_MESSAGE(1402018, new DescriptionId(paramItem1.getNameId()), new DescriptionId(paramItem2.getNameId())));
      return false;
    }
    int i = 0;
    if ((paramItem2.getEnchantLevel() > 10) && (paramItem2.getEnchantLevel() < 20) && (paramItem2.getPackCount() < paramItem2.getItemTemplate().getPackCount() + 1))
    {
      i++;
    }
    else if ((paramItem2.getEnchantLevel() > 20) && (paramItem2.getPackCount() < paramItem2.getItemTemplate().getPackCount() + 2))
    {
      i += 2;
    }
    else if ((paramItem2.getEnchantLevel() <= 10) && (paramItem2.getPackCount() > paramItem2.getItemTemplate().getPackCount()))
    {
      PacketSendUtility.sendPacket(paramPlayer, new SM_SYSTEM_MESSAGE(1402015, new DescriptionId(paramItem2.getNameId())));
      return false;
    }
    return (paramItem2.getPackCount() < paramItem2.getItemTemplate().getPackCount() + i) && (!paramItem2.isEquipped());
  }
  
  public UseTarget getTarget()
  {
    return target;
  }
  
  public void act(final Player paramPlayer, final Item paramItem1, final Item paramItem2)
  {
    final int i = paramItem1.getItemId();
    final int j = paramItem1.getObjectId();
    PacketSendUtility.broadcastPacket(paramPlayer, new SM_ITEM_USAGE_ANIMATION(paramPlayer.getObjectId(), paramItem1.getObjectId(), i, 5000, 0, 0), true);
    final ItemUseObserver local1 = new ItemUseObserver()
    {
      public void abort()
      {
        paramPlayer.getController().cancelTask(TaskId.ITEM_USE);
        paramPlayer.removeItemCoolDown(paramItem1.getItemTemplate().getUseLimits().getDelayId());
        PacketSendUtility.sendPacket(paramPlayer, new SM_SYSTEM_MESSAGE(1300427));
        PacketSendUtility.broadcastPacket(paramPlayer, new SM_ITEM_USAGE_ANIMATION(paramPlayer.getObjectId(), j, i, 0, 2, 0), true);
        paramPlayer.getObserveController().removeObserver(this);
      }
    };
    paramPlayer.getObserveController().attach(local1);
    paramPlayer.getController().addTask(TaskId.ITEM_USE, ThreadPoolManager.getInstance().schedule(new Runnable()
    {
      public void run()
      {
        paramPlayer.getObserveController().removeObserver(local1);
        PacketSendUtility.broadcastPacket(paramPlayer, new SM_ITEM_USAGE_ANIMATION(paramPlayer.getObjectId(), j, i, 0, 1, 1), true);
        if (!paramPlayer.getInventory().decreaseByObjectId(j, 1L)) {
          return;
        }
        int i = paramItem2.getPackCount();
        paramItem2.setPacked(true);
        paramItem2.setPackCount(++i);
        paramItem2.setPersistentState(PersistentState.UPDATE_REQUIRED);
        PacketSendUtility.sendPacket(paramPlayer, new SM_INVENTORY_UPDATE_ITEM(paramPlayer, paramItem2));
        PacketSendUtility.sendPacket(paramPlayer, new SM_SYSTEM_MESSAGE(1402031, new DescriptionId(paramItem2.getNameId())));
      }
    }, 5000L));
  }
}
