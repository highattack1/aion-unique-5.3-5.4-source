package com.aionemu.gameserver.model.templates.item.actions;

import com.aionemu.gameserver.controllers.ObserveController;
import com.aionemu.gameserver.controllers.PlayerController;
import com.aionemu.gameserver.controllers.observer.ItemUseObserver;
import com.aionemu.gameserver.model.TaskId;
import com.aionemu.gameserver.model.gameobjects.Item;
import com.aionemu.gameserver.model.gameobjects.player.Player;
import com.aionemu.gameserver.model.gameobjects.player.PlayerCommonData;
import com.aionemu.gameserver.model.items.storage.Storage;
import com.aionemu.gameserver.model.templates.item.ItemTemplate;
import com.aionemu.gameserver.network.aion.serverpackets.SM_ITEM_USAGE_ANIMATION;
import com.aionemu.gameserver.network.aion.serverpackets.SM_STATUPDATE_EXP;
import com.aionemu.gameserver.network.aion.serverpackets.SM_SYSTEM_MESSAGE;
import com.aionemu.gameserver.services.item.ItemService;
import com.aionemu.gameserver.utils.PacketSendUtility;
import com.aionemu.gameserver.utils.ThreadPoolManager;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlTransient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ExpExtractAction
  extends AbstractItemAction
{
  @XmlAttribute
  protected int cost;
  @XmlAttribute(name="percent")
  protected boolean isPercent;
  @XmlAttribute(name="item_id")
  protected int itemId;
  @XmlTransient
  private boolean isEventExp = false;
  @XmlTransient
  private Logger log = LoggerFactory.getLogger(ExpExtractAction.class);
  
  public ExpExtractAction() {}
  
  public boolean canAct(Player paramPlayer, Item paramItem1, Item paramItem2)
  {
    if (paramPlayer.getCommonData().getExp() == 0L) {
      return false;
    }
    return !paramPlayer.getInventory().isFull();
  }
  
  public void act(final Player paramPlayer, final Item paramItem1, Item paramItem2)
  {
    PacketSendUtility.sendPacket(paramPlayer, new SM_ITEM_USAGE_ANIMATION(paramPlayer.getObjectId(), paramItem1.getObjectId(), paramItem1.getItemTemplate().getTemplateId(), 5000, 0, 0));
    paramPlayer.getController().cancelTask(TaskId.ITEM_USE);
    final ItemUseObserver local1 = new ItemUseObserver()
    {
      public void abort()
      {
        paramPlayer.getController().cancelTask(TaskId.ITEM_USE);
        PacketSendUtility.sendPacket(paramPlayer, SM_SYSTEM_MESSAGE.STR_DECOMPOSE_ITEM_CANCELED(paramItem1.getNameId()));
        PacketSendUtility.sendPacket(paramPlayer, new SM_ITEM_USAGE_ANIMATION(paramPlayer.getObjectId(), paramItem1.getObjectId(), paramItem1.getItemTemplate().getTemplateId(), 0, 2, 0));
        paramPlayer.getObserveController().removeObserver(this);
      }
    };
    paramPlayer.getObserveController().attach(local1);
    paramPlayer.getController().addTask(TaskId.ITEM_USE, ThreadPoolManager.getInstance().schedule(new Runnable()
    {
      public void run()
      {
        paramPlayer.getObserveController().removeObserver(local1);
        int i = 0;
        if (isPercent) {
          i = (int)(paramPlayer.getCommonData().getExpNeed() / 100L) * cost;
        } else {
          i = cost;
        }
        //paramPlayer.getCommonData().setExp(paramPlayer.getCommonData().getExp() - i); // Todo
        ItemService.addItem(paramPlayer, itemId, 1L);
        paramPlayer.getInventory().decreaseByItemId(paramItem1.getItemId(), 1L);
        PacketSendUtility.sendPacket(paramPlayer, new SM_ITEM_USAGE_ANIMATION(paramPlayer.getObjectId(), paramItem1.getObjectId(), paramItem1.getItemTemplate().getTemplateId(), 0, 1, 0));
      }
    }, 5000L));
    PacketSendUtility.sendPacket(paramPlayer, new SM_STATUPDATE_EXP(paramPlayer.getCommonData().getExpShown(), paramPlayer.getCommonData().getExpRecoverable(), paramPlayer.getCommonData().getExpNeed(), paramPlayer.getCommonData().getCurrentReposteEnergy(), paramPlayer.getCommonData().getMaxReposteEnergy(), paramPlayer.getCommonData().getGoldenStarEnergy(), paramPlayer.getCommonData().getGrowthEnergy()));
  }
}
