package com.aionemu.gameserver.model.templates.item.actions;

import com.aionemu.gameserver.model.gameobjects.Item;
import com.aionemu.gameserver.model.gameobjects.player.Player;
import com.aionemu.gameserver.model.items.storage.Storage;
import com.aionemu.gameserver.model.templates.item.ItemTemplate;
import com.aionemu.gameserver.network.aion.serverpackets.SM_ITEM_USAGE_ANIMATION;
import com.aionemu.gameserver.network.aion.serverpackets.SM_SKILL_ANIMATION;
import com.aionemu.gameserver.utils.PacketSendUtility;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name="SkillAnimationAction")
public class SkillAnimationAction
  extends AbstractItemAction
{
  @XmlAttribute(name="test")
  protected int test;
  
  public SkillAnimationAction() {}
  
  public boolean canAct(Player paramPlayer, Item paramItem1, Item paramItem2)
  {
    return true;
  }
  
  public void act(Player paramPlayer, Item paramItem1, Item paramItem2)
  {
    ItemTemplate localItemTemplate = paramItem1.getItemTemplate();
    PacketSendUtility.broadcastPacket(paramPlayer, new SM_ITEM_USAGE_ANIMATION(paramPlayer.getObjectId(), paramItem1.getObjectId(), localItemTemplate.getTemplateId()), true);
    PacketSendUtility.sendPacket(paramPlayer, new SM_SKILL_ANIMATION(0, test));
    Item localItem = paramPlayer.getInventory().getItemByObjId(paramItem1.getObjectId());
    paramPlayer.getInventory().delete(localItem);
  }
}
