package com.aionemu.gameserver.model.templates.item.actions;

import com.aionemu.commons.database.dao.DAOManager;
import com.aionemu.gameserver.controllers.ObserveController;
import com.aionemu.gameserver.controllers.PlayerController;
import com.aionemu.gameserver.controllers.observer.ItemUseObserver;
import com.aionemu.gameserver.dao.ItemStoneListDAO;
import com.aionemu.gameserver.dataholders.DataManager;
import com.aionemu.gameserver.dataholders.ItemRandomBonusData;
import com.aionemu.gameserver.model.DescriptionId;
import com.aionemu.gameserver.model.TaskId;
import com.aionemu.gameserver.model.gameobjects.Item;
import com.aionemu.gameserver.model.gameobjects.PersistentState;
import com.aionemu.gameserver.model.gameobjects.player.Equipment;
import com.aionemu.gameserver.model.gameobjects.player.Player;
import com.aionemu.gameserver.model.items.IdianStone;
import com.aionemu.gameserver.model.items.RandomBonusResult;
import com.aionemu.gameserver.model.items.storage.Storage;
import com.aionemu.gameserver.model.templates.item.ItemTemplate;
import com.aionemu.gameserver.model.templates.item.ItemUseLimits;
import com.aionemu.gameserver.model.templates.item.bonuses.StatBonusType;
import com.aionemu.gameserver.network.aion.serverpackets.SM_INVENTORY_UPDATE_ITEM;
import com.aionemu.gameserver.network.aion.serverpackets.SM_ITEM_USAGE_ANIMATION;
import com.aionemu.gameserver.network.aion.serverpackets.SM_SYSTEM_MESSAGE;
import com.aionemu.gameserver.utils.PacketSendUtility;
import com.aionemu.gameserver.utils.ThreadPoolManager;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name="PolishAction")
public class PolishAction
  extends AbstractItemAction
{
  @XmlAttribute(name="set_id")
  protected int polishSetId;
  
  public PolishAction() {}
  
  public boolean canAct(Player paramPlayer, Item paramItem1, Item paramItem2)
  {
    if (paramItem1.getItemTemplate().getLevel() > paramItem2.getItemTemplate().getLevel())
    {
      PacketSendUtility.sendPacket(paramPlayer, new SM_SYSTEM_MESSAGE(1401649));
      return false;
    }

   if (paramItem2.hasTune())
    {
      PacketSendUtility.sendPacket(paramPlayer, new SM_SYSTEM_MESSAGE(1401750));
      return false;
    }
    return (!paramPlayer.isAttackMode()) && (paramItem2.getItemTemplate().isWeapon()) && (paramItem2.getItemTemplate().isCanPolish());
  }

  
  public void act(final Player paramPlayer, final Item paramItem1, final Item paramItem2)
  {
    final int i = paramItem1.getItemId();
    final int j = paramItem1.getObjectId();
    final int k = paramItem1.getNameId();
    PacketSendUtility.broadcastPacket(paramPlayer, new SM_ITEM_USAGE_ANIMATION(paramPlayer.getObjectId(), paramItem1.getObjectId(), i, 5000, 0, 0), true);
    final ItemUseObserver local1 = new ItemUseObserver()
    {
      public void abort()
      {
        paramPlayer.getController().cancelTask(TaskId.ITEM_USE);
        paramPlayer.removeItemCoolDown(paramItem1.getItemTemplate().getUseLimits().getDelayId());
        PacketSendUtility.sendPacket(paramPlayer, new SM_SYSTEM_MESSAGE(1300427));
        PacketSendUtility.broadcastPacket(paramPlayer, new SM_ITEM_USAGE_ANIMATION(paramPlayer.getObjectId(), j, i, 0, 2, 0), true);
        paramPlayer.getObserveController().removeObserver(this);
      }
    };
    paramPlayer.getObserveController().attach(local1);
    paramPlayer.getController().addTask(TaskId.ITEM_USE, ThreadPoolManager.getInstance().schedule(new Runnable()
    {
      public void run()
      {
        paramPlayer.getObserveController().removeObserver(local1);
        PacketSendUtility.broadcastPacket(paramPlayer, new SM_ITEM_USAGE_ANIMATION(paramPlayer.getObjectId(), j, i, 0, 1, 1), true);
        if (!paramPlayer.getInventory().decreaseByObjectId(j, 1L)) {
          return;
        }
        RandomBonusResult localRandomBonusResult = DataManager.ITEM_RANDOM_BONUSES.getRandomModifiers(StatBonusType.POLISH, polishSetId);
        if (localRandomBonusResult == null)
        {
          PacketSendUtility.sendPacket(paramPlayer, SM_SYSTEM_MESSAGE.STR_ENCHANT_ITEM_FAILED(new DescriptionId(k)));
          return;
        }
        PacketSendUtility.sendPacket(paramPlayer, new SM_SYSTEM_MESSAGE(1401650, "[item_ex:" + paramItem2.getItemId() + ";" + paramItem2.getItemName() + "]"));
        IdianStone localIdianStone = paramItem2.getIdianStone();
        if (localIdianStone != null)
        {
          localIdianStone.onUnEquip(paramPlayer);
          paramItem2.setIdianStone(null);
          localIdianStone.setPersistentState(PersistentState.DELETED);
          ((ItemStoneListDAO)DAOManager.getDAO(ItemStoneListDAO.class)).storeIdianStones(localIdianStone);
        }
        localIdianStone = new IdianStone(i, PersistentState.NEW, paramItem2, localRandomBonusResult.getTemplateNumber(), 1000000);
        PacketSendUtility.sendPacket(paramPlayer, SM_SYSTEM_MESSAGE.STR_MSG_POLISH_SUCCEED(paramItem2.getItemTemplate().getNameId()));
        paramItem2.setIdianStone(localIdianStone);
        if (paramItem2.isEquipped())
        {
          localIdianStone.onEquip(paramPlayer);
          paramPlayer.getEquipment().setPersistentState(PersistentState.UPDATE_REQUIRED);
        }
        PacketSendUtility.sendPacket(paramPlayer, new SM_INVENTORY_UPDATE_ITEM(paramPlayer, paramItem2));
      }
    }, 5000L));
  }
  
  public int getPolishSetId()
  {
    return polishSetId;
  }
}
