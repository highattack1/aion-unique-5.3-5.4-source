/*
 * This file is part of Aion-Unique. **Aion-Unique**
 *
 *  Aion-Unique is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Aion-Unique is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser Public License
 *  along with Aion-Unique.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.aionemu.gameserver.services.instance;

import com.aionemu.commons.network.util.ThreadPoolManager;
import com.aionemu.commons.services.CronService;
import com.aionemu.gameserver.configs.main.AutoGroupConfig;
import com.aionemu.gameserver.model.gameobjects.player.Player;
import com.aionemu.gameserver.network.aion.serverpackets.SM_AUTO_GROUP;
import com.aionemu.gameserver.network.aion.serverpackets.SM_SYSTEM_MESSAGE;
import com.aionemu.gameserver.services.AutoGroupService;
import com.aionemu.gameserver.utils.PacketSendUtility;
import com.aionemu.gameserver.world.World;
import java.util.Iterator;
import javolution.util.FastList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @Author Ghostfur
 */
public class GoldenArenaService
{
  private static final Logger log = LoggerFactory.getLogger(GoldenArenaService.class);
  public static final byte minlevel = 66;
  public static final byte maxlevel = 75;
  private boolean registerAvailable;
  private FastList<Integer> playersWithCooldown;
  public static final int maskId = 125;
  public static final int InstanceMapId = 302320000;

  public GoldenArenaService()
  {
    playersWithCooldown = new FastList();
  }

  public static GoldenArenaService getInstance()
  {
    return SingletonHolder.instance;
  }

  public void initGoldenArena()
  {
    String[] times = AutoGroupConfig.GOLDENARENA_TIMES.split("\\|");
    for (String cron : times) {
      CronService.getInstance().schedule(new Runnable()
      {
        public void run() {
          GoldenArenaService.this.startRegistration();
        }
      }
      , cron);
     }
  }

  private void startRegistration() {
    this.registerAvailable = true;
    ScheduleUnregistration();
    Iterator iter = World.getInstance().getPlayersIterator();
    while (iter.hasNext()) {
      Player player = (Player)iter.next();
      if ((player.getLevel() > 66) && (player.getLevel() < 75) &&
        (!isInInstance(player))) {
        PacketSendUtility.sendPacket(player, new SM_AUTO_GROUP(125, ( (byte) 6 )));
        PacketSendUtility.sendPacket(player, SM_SYSTEM_MESSAGE.STR_MSG_INSTANCE_OPEN_IDTM_Lobby01);
      }
    }
  }

  private void ScheduleUnregistration()
  {
    ThreadPoolManager.getInstance().schedule(new Runnable()
    {
      public void run() {
        registerAvailable = false;
        playersWithCooldown.clear();
        AutoGroupService.getInstance().unRegisterInstance(125);
        Iterator iter = World.getInstance().getPlayersIterator();
        while (iter.hasNext()) {
          Player player = (Player)iter.next();
          if (player.getLevel() > 66)
           PacketSendUtility.sendPacket(player, new SM_AUTO_GROUP(125, ( (byte) 6 ), true));
        }
      }
    }
    , AutoGroupConfig.GOLDENARENA_TIMER * 60 * 1000);
  }



  public byte getMinLevel()
  {
    return 66;
  }

  public byte getMaxLevel() {
    return 75;
  }

  public boolean isGoldenArenaAvailable() {
    return registerAvailable;
  }

  public void addCoolDown(Player player) {
    playersWithCooldown.add(player.getObjectId());
  }

  public boolean hasCoolDown(Player player) {
    return playersWithCooldown.contains(player.getObjectId());
  }

  public void showWindow(Player player, byte instanceMaskId) {
    if (!this.playersWithCooldown.contains(player.getObjectId()))
      PacketSendUtility.sendPacket(player, new SM_AUTO_GROUP(instanceMaskId));
  }

  private boolean isInInstance(Player player)
  {
    return player.isInInstance();
  }

  public boolean canPlayerJoin(Player player) {
    return (registerAvailable) && (player.getLevel() > 66) && (player.getLevel() < 75) && (!hasCoolDown(player)) && (!isInInstance(player));
  }

  private static class SingletonHolder
  {
    protected static final GoldenArenaService instance = new GoldenArenaService();
  }
}