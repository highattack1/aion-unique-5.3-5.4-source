package com.aionemu.gameserver.network.aion.serverpackets;

import com.aionemu.gameserver.network.aion.AionConnection;
import com.aionemu.gameserver.network.aion.AionServerPacket;

public class SM_SKILL_ANIMATION
  extends AionServerPacket
{
  private int action;
  private int test;
  
  public SM_SKILL_ANIMATION(int paramInt)
  {
    action = paramInt;
  }
  
  public SM_SKILL_ANIMATION(int paramInt1, int paramInt2)
  {
    action = paramInt1;
    test = paramInt2;
  }
  
  protected void writeImpl(AionConnection paramAionConnection)
  {
    switch (action)
    {
    case 0: 
      writeC(0);
      writeH(1);
      writeC(test);
      writeH(0);
      writeD(0);
      break;
    case 1: 
      writeC(1);
      writeH(0);
    }
  }
}
