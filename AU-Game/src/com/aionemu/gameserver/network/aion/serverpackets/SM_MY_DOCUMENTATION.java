package com.aionemu.gameserver.network.aion.serverpackets;

import com.aionemu.gameserver.network.aion.AionConnection;
import com.aionemu.gameserver.network.aion.AionServerPacket;

public class SM_MY_DOCUMENTATION
  extends AionServerPacket
{
  public SM_MY_DOCUMENTATION() {}
  
  protected void writeImpl(AionConnection paramAionConnection)
  {
    writeD(1);
    writeD(0);
    writeD(0);
    writeD(37);
    writeD(0);
    writeD(0);
    writeD(37);
    writeD(0);
    writeD(37);
  }
}
