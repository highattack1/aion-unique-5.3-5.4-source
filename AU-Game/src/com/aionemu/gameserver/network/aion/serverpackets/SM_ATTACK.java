package com.aionemu.gameserver.network.aion.serverpackets;

import com.aionemu.gameserver.controllers.CreatureController;
import com.aionemu.gameserver.controllers.attack.AttackResult;
import com.aionemu.gameserver.controllers.attack.AttackStatus;
import com.aionemu.gameserver.model.gameobjects.Creature;
import com.aionemu.gameserver.model.gameobjects.player.Player;
import com.aionemu.gameserver.model.stats.container.CreatureLifeStats;
import com.aionemu.gameserver.network.aion.AionConnection;
import com.aionemu.gameserver.network.aion.AionServerPacket;
import java.util.List;

public class SM_ATTACK
		extends AionServerPacket
{
	private int attackno;
	private int time;
	private int type;
	private int SimpleAttackType;
	private List<AttackResult> attackList;
	private Creature attacker;
	private Creature target;

	public SM_ATTACK(Creature attacker, Creature target, int attackno, int time, int type, List<AttackResult> attackList)
	{
		this.attacker = attacker;
		this.target = target;
		this.attackno = attackno;
		this.time = time;
		this.type = type;
		this.attackList = attackList;
		SimpleAttackType = attacker.getController().getSimpleAttackType();
	}

	protected void writeImpl(AionConnection con)
	{
		writeD(attacker.getObjectId());
		writeC(attackno);
		writeH(time);
		writeC((byte)SimpleAttackType);
		writeC(type);
		writeD(target.getObjectId());

		int attackerMaxHp = attacker.getLifeStats().getMaxHp();
		int attackerCurrHp = attacker.getLifeStats().getCurrentHp();
		int targetMaxHp = target.getLifeStats().getMaxHp();
		int targetCurrHp = target.getLifeStats().getCurrentHp();

		writeC((int)(100.0F * targetCurrHp / targetMaxHp));
		writeC((int)(100.0F * attackerCurrHp / attackerMaxHp));

		switch (((AttackResult)attackList.get(0)).getAttackStatus().getId())
		{
			case 4:
			case 5:
			case 196:
			case 213:
				writeD(32);
				break;
			case 2:
			case 3:
			case 194:
			case 211:
				writeD(64);
				break;
			case 0:
			case 1:
			case 192:
			case 209:
				writeD(128);
				break;
			case 6:
			case 7:
			case 198:
			case 215:
				writeD(256);
				break;
			default:
				writeD(0);
		}


		if (((target instanceof Player)) &&
				(((AttackResult)attackList.get(0)).getAttackStatus().isCounterSkill())) {
			((Player)target).setLastCounterSkill(((AttackResult)attackList.get(0)).getAttackStatus());
		}

		writeC(attackList.size());
		for (AttackResult attack : attackList) {
			writeD(attack.getDamage());
			writeC(attack.getAttackStatus().getId());

			byte shieldType = (byte)attack.getShieldType();
			writeC(shieldType);

			switch (shieldType) {
				case 0:
				case 2:
					break;
				case 8:
				case 10:
					writeD(attack.getProtectorId());
					writeD(attack.getProtectedDamage());
					writeD(attack.getProtectedSkillId());
					break;
				case 16:
					writeD(0);
					writeD(0);
					writeD(0);
					writeD(0);
					writeD(0);
					writeD(attack.getShieldMp());
					writeD(attack.getReflectedSkillId());
					break;
				default:
					writeD(attack.getProtectorId());
					writeD(attack.getProtectedDamage());
					writeD(attack.getProtectedSkillId());
					writeD(attack.getReflectedDamage());
					writeD(attack.getReflectedSkillId());
					writeD(0);
					writeD(0);
			}

		}
		writeC(0);
	}
}
